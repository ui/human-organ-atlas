FROM node:20-alpine as build

COPY . /hoa
WORKDIR /hoa

RUN apk --no-cache add git \
  && npm install -g pnpm \
  && pnpm install \
  && pnpm build


FROM nginx:alpine

COPY --from=build /hoa/build /usr/share/nginx/html
COPY nginx.conf /etc/nginx/conf.d/default.conf

RUN apk --no-cache add curl
COPY ./healthcheck /

EXPOSE 80

HEALTHCHECK --start-period=10s --timeout=5s --retries=3 CMD /healthcheck
