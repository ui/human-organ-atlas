import React from 'react';
import PulseLoader from 'react-spinners/PulseLoader';

import { Box } from '../Primitives';

function Spinner() {
  return (
    <Box>
      <PulseLoader color="currentColor" />
    </Box>
  );
}

export default Spinner;
