import create from 'zustand';
import { persist } from 'zustand/middleware';

// const preset =
//   localStorage.getItem('isDark') === 'true' ||
//   window.matchMedia && window.matchMedia('(prefers-color-scheme: dark)').matches
//     ? true
//     : false;

export const useAppStore = create((set, get) => ({
  isDark: true, // preset,
  // toggleTheme: () => {
  //   const newTheme = !get().isDark;
  //   localStorage.setItem('isDark', newTheme);
  //   set(() => ({ isDark: newTheme }));
  // },

  loadOnScroll: false,
  toggleLoadOnScroll: () => {
    set(() => ({ loadOnScroll: !get().loadOnScroll }));
  },

  numResults: undefined,
  setNumResults: (numResults) => {
    set({ numResults });
  },
}));

export const useSearchStore = create((set, get) => ({
  search: '',
  setSearch: (search) => {
    if (search !== get().search) {
      set({ search });
    }
  },
}));

export const useCitationStore = create(
  persist(
    (set, get) => ({
      modalSeen: false,
      setModalSeen: (modalSeen) => set({ modalSeen }),
    }),
    { name: 'citationStore' }
  )
);
