/* eslint-disable jsx-a11y/control-has-associated-label */
import { getAllValuesAtPath, NOOP } from '../App/helpers';
import { useAllDatasets } from '../App/hooks';
import { Text, Flex } from '../Primitives';
import { useListQueryParam } from '../router-utils';
import FilterBox from './FilterBox';

function ListPicker(props) {
  const { name, label, path, transformValue = NOOP } = props;
  const param = useListQueryParam(name);

  const { datasets } = useAllDatasets();
  const options = new Set(
    getAllValuesAtPath(datasets, path).map(transformValue)
  );

  return (
    <FilterBox
      title={label}
      showTitle={label !== false}
      isActive={param.isActive}
      onClear={() => param.remove()}
    >
      <Flex column gap={1}>
        {[...options].map((word) => {
          const isSelected = param.values.includes(word);

          return (
            <Flex
              key={word}
              as="label"
              sx={{
                alignItems: 'center',
                color: isSelected && 'textVivid',
                fontSize: [0, 0, 0, 0, 1],
                fontWeight: isSelected && 'bold',
                cursor: 'pointer',
                ':hover': { textDecoration: 'underline' },
              }}
            >
              <input
                type="checkbox"
                checked={isSelected}
                onChange={() => param.toggleValue(word)}
                style={{ cursor: 'inherit' }}
              />
              <Text flex="1 1 0%" ml={2}>
                {word}
              </Text>
            </Flex>
          );
        })}
      </Flex>
    </FilterBox>
  );
}

export default ListPicker;
