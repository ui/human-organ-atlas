import { useDebouncedCallback } from '@react-hookz/web';
import { Input } from '@rebass/forms/styled-components';
import { useState } from 'react';
import { FiXCircle } from 'react-icons/fi';

import { Flex, Button } from '../Primitives';
import { useQueryParam } from '../router-utils';
import FilterBox from './FilterBox';

function TextInput(props) {
  const { name, label, placeholder } = props;

  const param = useQueryParam(name);
  const [inputValue, setInputValue] = useState(param.value || '');

  const handleChange = useDebouncedCallback(
    (val) => param.setValue(val),
    [param],
    500
  );

  return (
    <FilterBox
      title={label}
      showTitle={label !== false}
      isActive={param.isActive}
    >
      <Flex>
        <Input
          px={2}
          value={inputValue}
          placeholder={placeholder}
          onChange={(evt) => {
            const { value } = evt.target;
            setInputValue(value);
            handleChange(value);
          }}
        />
        <Button
          variant="action"
          disabled={!inputValue}
          aria-label="Clear"
          onClick={() => {
            setInputValue('');
            param.remove();
          }}
        >
          <FiXCircle />
        </Button>
      </Flex>
    </FilterBox>
  );
}

export default TextInput;
